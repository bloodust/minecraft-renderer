#include "face.hpp"

namespace {
	const glm::vec3 UP(0, 1, 0);
	const glm::vec3 DOWN(0, -1, 0);
	const glm::vec3 LEFT(-1, 0, 0);
	const glm::vec3 RIGHT(1, 0, 0);
	const glm::vec3 FORWARD(0, 0, 1);
	const glm::vec3 BACKWARD(0, 0, -1);
	void fix_face_position(glm::vec4 &position, const glm::vec3& normal){
		if (normal == UP)
			position += glm::vec4(0, 1, 0, 0);
		else if (normal == DOWN)
			position += glm::vec4(0, 0, 1, 0);
		else if (normal == LEFT)
			position += glm::vec4(0, 1, 0, 0);
		else if (normal == RIGHT)
			position += glm::vec4(1, 0, 0, 0);
		else if (normal == FORWARD)
			position += glm::vec4(0, 0, 1, 0);
		else if (normal == BACKWARD)
			position += glm::vec4(1, 0, 0, 0);
	}
}
Face::Face(glm::vec3 position, glm::vec3 normal, unsigned char texture_id) :
position(position, texture_id),
normal(normal)
{

}
Face::Face(glm::vec4 position, glm::vec3 normal) :
position(position),
normal(normal)
{
	
}

void Face::get_geometry(std::vector<glm::vec4> &vertices, std::vector<glm::vec3> &normals, std::vector<glm::vec2>& tex_coords)const
{
	using namespace glm;
	vec4 right = vec4(normal.z, normal.x, normal.y, 0);
	vec4 top = vec4(cross(normal, vec3(right)), 0);
	
	vec4 local_position = position;
	fix_face_position(local_position, normal);

	vertices.push_back(local_position);
	normals.push_back(normal);
	tex_coords.push_back(vec2(0, 0));

	vertices.push_back(local_position + right);
	normals.push_back(normal);
	tex_coords.push_back(vec2(0, 1));

	vertices.push_back(local_position + top);
	normals.push_back(normal);
	tex_coords.push_back(vec2(1, 0));

	vertices.push_back(local_position + right);
	normals.push_back(normal);
	tex_coords.push_back(vec2(0, 1));

	vertices.push_back(local_position + right + top);
	normals.push_back(normal);
	tex_coords.push_back(vec2(1, 1));

	vertices.push_back(local_position + top);
	normals.push_back(normal);
	tex_coords.push_back(vec2(1, 0));
}