#ifndef CUBE_GEOMETRY_OPTIMIZER_HPP
#define CUBE_GEOMETRY_OPTIMIZER_HPP
#include "chunk.hpp"
#include "utils/Graphics/vertex.hpp"
#include "face.hpp"
#include <vector>
#include <glm/glm.hpp>

class CubeOptimizer
{
public:
	friend CubeOptimizer& operator <<(CubeOptimizer& left, const std::vector<glm::vec4> &points); //write
	friend CubeOptimizer& operator >>(CubeOptimizer& left, std::vector<glm::vec4> &points); //read, auto optimizes on read, inserts optimized points to &points, clears internal buffer

private:
	void optimize_buffer();
	std::vector<glm::vec4> get_neighbour_points(glm::vec4 point);

	std::vector<glm::vec4> buffer;
};
class ChunkOptimizer
{
public:
	friend ChunkOptimizer& operator <<(ChunkOptimizer& left, const Chunk &chunk); //write
	friend ChunkOptimizer& operator <<(ChunkOptimizer& left, const std::vector<Chunk> &chunks); //write
	friend ChunkOptimizer& operator >>(ChunkOptimizer& left, std::vector<glm::vec4> &points); //read, auto optimizes on read, inserts optimized points to &points, clears internal buffer
	friend ChunkOptimizer& operator >>(ChunkOptimizer& left, std::vector<Face> &faces); //read, auto optimizes on read, inserts optimized points to &points, clears internal buffer
	void do_greedy(std::vector<Vertex2>& vertices, std::vector<unsigned int>& faces);
	void greedy(const Chunk& chunk, std::vector<Vertex2> &vertices, std::vector<glm::ivec4> &faces);
private:
	std::vector<glm::vec4> get_optimized_geometry()const;
	std::vector<Chunk> get_neighbour_chunks(const Chunk& chunk)const;
	std::vector<Face> get_faces();

	std::vector<Chunk> buffer;
};

#endif
