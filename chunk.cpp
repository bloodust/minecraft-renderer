#include "chunk.hpp"

std::vector<glm::vec4> Chunk::get_cubes()const
{
	std::vector<glm::vec4> cubes;
	int n = 0;
	for(int x = 0; x != WIDTH; ++x){
		for(int z = 0; z != DEPTH; ++z){
			for(int y = 0; y != HEIGHT; ++y)
				cubes.push_back(glm::vec4(position, 0) + glm::vec4(x, y, z, this->cubes[n++]));
		}
	}
	return cubes;
}
std::vector<glm::vec4> Chunk::get_visible_cubes()const
{
	std::vector<glm::vec4> cubes;
	int n = 0;
	for(int x = 0; x != WIDTH; ++x){
		for(int z = 0; z != DEPTH; ++z){
			for(int y = 0; y != HEIGHT; ++y){
				if(this->cubes[n] != 0 && get_cube_neighbours(glm::vec4(x,y,z,0.0f)).size() < 6)
					cubes.push_back(glm::vec4(position, 0)+glm::vec4(x, y, z, this->cubes[n++]));
			}
		}
	}
	return cubes;
}
glm::vec4 Chunk::get_cube_relative(glm::vec3 position)const
{
	if(is_inside_chunk_relative(position)){
		unsigned int index = static_cast<unsigned int>(position.x*WIDTH*DEPTH + position.z*DEPTH + position.y);
		return glm::vec4(position, cubes[index]);
	}
	return glm::vec4(position, 0);
}
glm::vec4 Chunk::get_cube(glm::vec3 position)const
{
	if(is_inside_chunk_absolute(position)){
		position -= this->position;
		unsigned int index = static_cast<unsigned int>(position.x*WIDTH*DEPTH + position.z*DEPTH + position.y);
		return glm::vec4(position + this->position, cubes[index]);
	}
	return glm::vec4(position, 0);
}
bool Chunk::is_inside_chunk_relative(glm::vec3 position)const
{
	return position.x >= 0 && position.x < WIDTH
		&& position.y >= 0 && position.y < HEIGHT
		&& position.z >= 0 && position.z < DEPTH;
}
bool Chunk::is_inside_chunk_absolute(glm::vec3 position)const
{
	auto p = this->position;
	return position.x >= p.x && position.x < (p.x+WIDTH)
		&& position.y >= p.y && position.y < (p.y+HEIGHT)
		&& position.z >= p.z && position.z < (p.z+DEPTH);
}
std::vector<glm::vec4> Chunk::get_cube_neighbours(glm::vec4 cube)const
{
	std::vector<glm::vec4> neighbours;
	int x[] = { -1, 0, 1, 0, 0, 0 };
	int y[] = { 0, 0, 0, 0, -1, 1 };
	int z[] = { 0, 1, 0, -1, 0, 0 };

	for(int i = 0; i != 6; ++i){
		glm::vec3 cube_to_find(glm::vec3(cube) + glm::vec3(x[i], y[i], z[i]));
		auto found = get_cube_relative(cube_to_find);
		if(found.w != 0)
			neighbours.push_back(found);
	}
	return neighbours;
}
void Chunk::set_cube(glm::vec4 cube)
{
	glm::vec3 position = glm::vec3(cube);
	if (is_inside_chunk_absolute(position)){
		position -= this->position;
		unsigned int index = static_cast<unsigned int>(position.x*WIDTH*DEPTH + position.z*DEPTH + position.y);
		cubes[index] = static_cast<unsigned char>(cube.w);
	}
}
glm::vec3 Chunk::get_chunk_coordinate(const glm::vec4& position)
{
	glm::ivec3 integer = glm::ivec3(position);
	if (integer.x < 0)
		integer.x -= WIDTH-1;
	if (integer.y < 0)
		integer.y -= HEIGHT-1;
	if (integer.z < 0)
		integer.z -= DEPTH-1;

	return glm::vec3(WIDTH, HEIGHT, DEPTH)*glm::vec3(integer.x / (int)WIDTH, integer.y / (int)HEIGHT, integer.z / (int)DEPTH);
}
std::vector<Chunk> Chunk::chunkify(const std::vector<glm::vec4>& cubes)
{
	std::vector<Chunk> existing_chunks;
	chunkify(cubes, existing_chunks);
	return existing_chunks;
}
void Chunk::chunkify(const std::vector<glm::vec4>& cubes, std::vector<Chunk>& existing_chunks)
{
	for (auto iter = cubes.begin(); iter != cubes.end(); ++iter){
		bool inserted = false;
		//see if we already have chunk for this cube
		for (auto iter2 = existing_chunks.begin(); iter2 != existing_chunks.end(); ++iter2){
			if (iter2->is_inside_chunk_absolute(glm::vec3(*iter))){
				iter2->set_cube(*iter);
				inserted = true;
				break;
			}
		}
		//we dont have a chunk for this cube = create new one
		if (!inserted){
			Chunk chunk(get_chunk_coordinate(*iter), 0);
			chunk.set_cube(*iter);
			existing_chunks.emplace_back(chunk);
		}
	}
}