#ifndef CAVE_GAME_HPP
#define CAVE_GAME_HPP

#include "cube_renderer.hpp"

#include "utils/camera.hpp"
#include "utils/graphics/model_manager.hpp"
#include "utils/graphics/texture_manager.hpp"
#include "utils/graphics/shader_manager.hpp"
#include "utils/file_watcher.hpp"
#include "utils/Graphics/mssao.hpp"
#include "utils/Graphics/downsample.hpp"

#include <glm/glm.hpp>
#include <SFML/Window.hpp>
#include <string>
#include <vector>


class Game
{
public:
	Game();
	void initialize(int resolution_width, int resolution_height);
	void render();
	void update();
	bool is_requiring_mouse_capture()const;
	void resize(int resolution_width, int resolution_height);
	void mouse_move(float deltax, float deltay);
	void input(const sf::Event &event, sf::Window& window); //window is for debugging purposes
	~Game();
private:
	void check_for_changed_files();
	glm::mat4 get_projection_matrix()const;
	void render_postprocess();

	float fov, camera_near, camera_far;
	glm::vec2 resolution;
	glm::mat4 MVP, inverse_MVP;

	MovingCamera free_camera;
	ShaderManager shader_manager;
	TextureManager texture_manager;
	ModelManager model_manager;
	FileWatcher shader_file_watcher;

	//post processing
	VertexBuffer full_screen_quad;
	PostProcess default_postprocess;
	MSSAO mssao;
	MSSAODownsample half_downsample, quarter_downsample, eighth_downsample, sixteenth_downsample;


	PointRenderer cube_renderer;
	FaceRenderer face_renderer;
	TriangleRenderer triangle_renderer;

	unsigned int texture_array_id;
};


#endif

