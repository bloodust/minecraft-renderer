#ifndef CUBE_ILLUMINATOR_HPP
#define CUBE_ILLUMINATOR_HPP
#include "chunk.hpp"
#include <vector>

class CubeIlluminator
{
public:
	friend CubeIlluminator& operator << (CubeIlluminator& left, const std::vector<Chunk> &chunks);
	friend CubeIlluminator& operator << (CubeIlluminator& left, const Chunk &chunk);
	friend CubeIlluminator& operator >> (CubeIlluminator& left, std::vector<Chunk> &chunks);
private:
	void calculate_lighting();
	void calculate_ambient_occlusion();
	std::vector<Chunk> buffer;
};

#endif
