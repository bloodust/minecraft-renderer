#include "geometry_optimizer.hpp"
#include <algorithm>
#include <iostream>

bool sortvec3(const glm::vec3 &left, const glm::vec3 &right)
{
	if(left.x == right.x){
		if(left.y == right.y)
			return left.z < right.z;
		else
			return left.y < right.y;
	}
	return left.x < right.x;
}
bool sortvec4(const glm::vec4 &left, const glm::vec4 &right)
{
	if(left.x == right.x){
		if(left.y == right.y)
			return left.z < right.z;
		else
			return left.y < right.y;
	}
	return left.x < right.x;
}
CubeOptimizer& operator << (CubeOptimizer& left, const std::vector<glm::vec4> &points)
{
	left.buffer.insert(left.buffer.begin(), points.begin(), points.end());
	return left;
}
CubeOptimizer& operator >> (CubeOptimizer& left, std::vector<glm::vec4> &points)
{
	std::sort(left.buffer.begin(), left.buffer.end(), sortvec4);
	left.optimize_buffer();
	points.insert(points.end(), left.buffer.begin(), left.buffer.end());
	return left;
}

std::vector<glm::vec4> CubeOptimizer::get_neighbour_points(glm::vec4 point)
{
	std::vector<glm::vec4> neighbours;
	int x[] = { -1, 0, 1, 0, 0, 0 };
	int y[] = { 0, 0, 0, 0, -1, 1 };
	int z[] = { 0, 1, 0, -1, 0, 0 };

	for(int i = 0; i != 6; ++i){
		glm::vec4 point_to_find(point + glm::vec4(x[i], y[i], z[i], 0));
		if(std::binary_search(buffer.begin(), buffer.end(), point_to_find, sortvec4))
			neighbours.push_back(point_to_find);
	}
	return neighbours;
}
void CubeOptimizer::optimize_buffer()
{
	std::vector<glm::vec4> optimized;
	optimized.reserve(buffer.size());
	for(auto iter = buffer.begin(); iter != buffer.end(); ++iter){
		if(get_neighbour_points(*iter).size() < 6)
			optimized.push_back(*iter);
	}
	buffer = optimized;
}




ChunkOptimizer& operator << (ChunkOptimizer& left, const std::vector<Chunk> &chunks)
{
	left.buffer.insert(left.buffer.begin(), chunks.begin(), chunks.end());
	return left;
}
ChunkOptimizer& operator <<(ChunkOptimizer& left, const Chunk &chunk)
{
	left.buffer.push_back(chunk);
	return left;
}
ChunkOptimizer& operator >> (ChunkOptimizer& left, std::vector<glm::vec4> &points)
{
	auto optimized_geometry = left.get_optimized_geometry();
	points.insert(points.end(), optimized_geometry.begin(), optimized_geometry.end());
	return left;
}
ChunkOptimizer& operator >>(ChunkOptimizer& left, std::vector<Face> &faces)
{
	auto faces_out = left.get_faces();
	faces.insert(faces.end(), faces_out.begin(), faces_out.end());
	return left;
}

std::vector<Chunk> ChunkOptimizer::get_neighbour_chunks(const Chunk& chunk)const
{
	std::vector<Chunk> neighbours;
	int x[] = { -1, 0, 1, 0, 0, 0 };
	int y[] = { 0, 0, 0, 0, -1, 1 };
	int z[] = { 0, 1, 0, -1, 0, 0 };

	for(int i = 0; i != 6; ++i){
		glm::vec3 chunk_to_find(chunk.position + glm::vec3(x[i], y[i], z[i])*glm::vec3(16,16,16));
		bool was_found = false;
		for(auto iter = buffer.begin(); iter != buffer.end(); ++iter){
			if(chunk_to_find == iter->position){
				neighbours.push_back(*iter);
				was_found = true;
				break;
			}
		}
		if(!was_found) //add air cunk
			neighbours.emplace_back(Chunk(chunk_to_find, 0));
	}
	return neighbours;
}
std::vector<glm::vec4> get_more_optimized_geometry(const Chunk& chunk, const std::vector<Chunk>& chunk_neighbours)
{
	auto cubes = chunk.get_visible_cubes();
	std::vector<glm::vec4> optimized;
	
	for(auto iter2 = cubes.begin(); iter2 != cubes.end(); ++iter2){
		bool next_to_air = false;
		bool was_inside_chunk = false;
		std::vector<glm::vec4> near_cubes;
		for(auto iter = chunk_neighbours.begin(); iter != chunk_neighbours.end(); ++iter){
			glm::vec3 direction = glm::max(glm::vec3(-1,-1,-1),glm::min(iter->position - chunk.position, glm::vec3(1, 1, 1)));
		
			glm::vec3 test_position = glm::vec3(*iter2) + direction;
			if(iter->is_inside_chunk_absolute(test_position)){
				was_inside_chunk = true;
				auto cube = iter->get_cube(test_position);
				if(cube.w == 0){
					next_to_air = true;
					break;
				}
			}
			else if(chunk.is_inside_chunk_absolute(test_position))
				;
			else //outside all blocks
				next_to_air = true; //not possible
		}
		if(next_to_air)
			optimized.push_back(*iter2);
	}
	return optimized;
}
std::vector<glm::vec4> ChunkOptimizer::get_optimized_geometry()const
{
	std::vector<glm::vec4> optimized;
	optimized.reserve(buffer.size()*Chunk::WIDTH*Chunk::HEIGHT*Chunk::DEPTH);
	for(auto iter = buffer.begin(); iter != buffer.end(); ++iter){
		auto chunk_neighbours = get_neighbour_chunks(*iter);
		int before = iter->get_visible_cubes().size();
		auto cubes = get_more_optimized_geometry(*iter, chunk_neighbours);
		int after = cubes.size();
		int diff = before - after;
		optimized.insert(optimized.end(), cubes.begin(), cubes.end());
	}
	return optimized;
}

std::vector<Face> get_more_optimized_geometry_faces(const Chunk& chunk, const std::vector<Chunk>& chunk_neighbours)
{
	auto cubes = chunk.get_visible_cubes();
	std::vector<Face> optimized;

	for (auto iter2 = cubes.begin(); iter2 != cubes.end(); ++iter2){
		bool next_to_air = false;
		bool was_inside_chunk = false;
		std::vector<glm::vec3> faces_to_save; //normals == face directions
		for (auto iter = chunk_neighbours.begin(); iter != chunk_neighbours.end(); ++iter){
			glm::vec3 direction = glm::max(glm::vec3(-1, -1, -1), glm::min(iter->position - chunk.position, glm::vec3(1, 1, 1)));

			glm::vec3 test_position = glm::vec3(*iter2) + direction;
			if (iter->is_inside_chunk_absolute(test_position)){
				was_inside_chunk = true;
				auto cube = iter->get_cube(test_position);
				if (cube.w == 0){
					next_to_air = true;
					faces_to_save.push_back(direction);
					//break;
				}
			}
			else if (chunk.is_inside_chunk_absolute(test_position))
				;
			else //outside all blocks
				next_to_air = true; //not possible
		}
		for (auto iter = faces_to_save.begin(); iter != faces_to_save.end();++iter)
			optimized.push_back(Face(*iter2,*iter));
	}
	return optimized;
}
std::vector<Face> ChunkOptimizer::get_faces()
{
	std::vector<Face> faces;
	faces.reserve(buffer.size()*Chunk::WIDTH*Chunk::HEIGHT*Chunk::DEPTH);
	for (auto iter = buffer.begin(); iter != buffer.end(); ++iter){
		auto chunk_neighbours = get_neighbour_chunks(*iter);
		int before = iter->get_visible_cubes().size();
		auto subfaces = get_more_optimized_geometry_faces(*iter, chunk_neighbours);
		faces.insert(faces.end(), subfaces.begin(), subfaces.end());
	}
	return faces;
}

void ChunkOptimizer::do_greedy(std::vector<Vertex2>& vertices, std::vector<unsigned int>& indices)
{
	for (auto iter = buffer.begin(); iter != buffer.end(); ++iter){
		std::vector<glm::ivec4> faces;
		int s = vertices.size();
		greedy(*iter, vertices, faces);

		//fix newly inserted vertex positions
		//for (auto iter2 = vertices.begin()+s; iter2 != vertices.end(); ++iter2)
			//iter2->vertex += glm::vec4(iter->position, 0);
		for (auto iter = faces.begin(); iter != faces.end(); ++iter){
			indices.push_back(iter->x);
			indices.push_back(iter->y);
			indices.push_back(iter->z);

			indices.push_back(iter->x);
			indices.push_back(iter->z);
			indices.push_back(iter->w);
		}
		
	}
}

void ChunkOptimizer::greedy(const Chunk& chunk, std::vector<Vertex2> &vertices, std::vector<glm::ivec4> &faces)
{
	const int dims[3] = { Chunk::WIDTH, Chunk::HEIGHT, Chunk::DEPTH };
	for (std::size_t axis = 0; axis < 3; ++axis)
	{
		const std::size_t u = (axis + 1) % 3;
		const std::size_t v = (axis + 2) % 3;

		int x[3] = { 0 }, q[3] = { 0 };
		std::vector<int> mask(dims[u] * dims[v]);

		// Compute mask
		q[axis] = 1;
		for (x[axis] = -1; x[axis] < dims[axis];)
		{
			std::size_t counter = 0;
			for (x[v] = 0; x[v] < dims[v]; ++x[v]){
				for (x[u] = 0; x[u] < dims[u]; ++x[u], ++counter)
				{
					const int a = 0 <= x[axis] ? static_cast<int>(chunk.get_cube(glm::vec3(x[0], x[1], x[2]) + chunk.position).w) : 0;
					const int b = x[axis] < dims[axis] - 1 ? static_cast<int>(chunk.get_cube(glm::vec3(x[0] + q[0],
						x[1] + q[1],
						x[2] + q[2]) + chunk.position).w) : 0;
					const bool ba = a != 0;
					if (ba == (b != 0))
						mask[counter] = 0;
					else if (ba)
						mask[counter] = a;
					else
						mask[counter] = -b;
				}
			}
			++x[axis];

			// Generate mesh for mask using lexicographic ordering
			std::size_t width = 0, height = 0;

			counter = 0;
			for (std::size_t j = 0; j < dims[v]; ++j)
			for (std::size_t i = 0; i < dims[u];)
			{
				int c = mask[counter];
				if (c)
				{
					// Compute width
					for (width = 1; counter + width < dims[u] * dims[v] 
						&& c == mask[counter + width] 
						&& i + width < dims[u]; ++width)
					{
					}

					// Compute height
					bool done = false;
					for (height = 1; j + height < dims[v]; ++height)
					{
						for (std::size_t k = 0; k < width; ++k)
							if (c != mask[counter + k + height * dims[u]])
							{
								done = true;
								break;
							}

						if (done)
							break;
					}

					// Add quad
					x[u] = i;
					x[v] = j;

					int du[3] = { 0 }, dv[3] = { 0 };

					if (c > 0)
					{
						dv[v] = height;
						du[u] = width;
					}
					else
					{
						c = -c;
						du[v] = height;
						dv[u] = width;
					}

					const std::size_t vertexSize = vertices.size();
					auto a = Vertex2(glm::vec4(x[0], x[1], x[2], c),glm::vec4(0,1,0,0),glm::vec2(0,0));
					auto b = Vertex2(glm::vec4(x[0] + du[0], x[1] + du[1], x[2] + du[2], c), glm::vec4(0,1,0,0), glm::vec2(0,1));
					auto cc = Vertex2(glm::vec4(x[0] + du[0] + dv[0], x[1] + du[1] + dv[1], x[2] + du[2] + dv[2], c), glm::vec4(0,1,0,0), glm::vec2(1, 1));
					auto d = Vertex2(glm::vec4(x[0] + dv[0], x[1] + dv[1], x[2] + dv[2], c), glm::vec4(0,1,0,0), glm::vec2(1,0));
					
					glm::vec3 normal = glm::normalize(glm::cross(glm::vec3(b.vertex - a.vertex), glm::vec3(cc.vertex - a.vertex)));
					//TODO Calculate ambient occlusion to normal.w component here
					a.normal = b.normal = cc.normal = d.normal = glm::vec4(normal,1);


					glm::vec2 texture_coord_multiplier = glm::vec2(1, 1);
					if (normal.x < 0)
						texture_coord_multiplier = glm::abs(glm::vec2(cc.vertex.y, cc.vertex.z) - glm::vec2(a.vertex.y, a.vertex.z));
					else if (normal.x > 0)
						texture_coord_multiplier = glm::abs(glm::vec2(cc.vertex.z, cc.vertex.y) - glm::vec2(a.vertex.z, a.vertex.y));
					else if (normal.y > 0)
						texture_coord_multiplier = glm::abs(glm::vec2(cc.vertex.x, cc.vertex.z) - glm::vec2(a.vertex.x, a.vertex.z));
					else if (normal.y < 0)
						texture_coord_multiplier = glm::abs(glm::vec2(cc.vertex.z, cc.vertex.x) - glm::vec2(a.vertex.z, a.vertex.x));
					else if (normal.z < 0)
						texture_coord_multiplier = glm::abs(glm::vec2(cc.vertex.x, cc.vertex.y) - glm::vec2(a.vertex.x, a.vertex.y));
					else if (normal.z > 0)
						texture_coord_multiplier = glm::abs(glm::vec2(cc.vertex.y, cc.vertex.x) - glm::vec2(a.vertex.y, a.vertex.x));

					a.tex_coord *= texture_coord_multiplier;
					b.tex_coord *= texture_coord_multiplier;
					cc.tex_coord *= texture_coord_multiplier;
					d.tex_coord *= texture_coord_multiplier;

					a.vertex += glm::vec4(chunk.position, 0);
					b.vertex += glm::vec4(chunk.position, 0);
					cc.vertex += glm::vec4(chunk.position, 0);
					d.vertex += glm::vec4(chunk.position, 0);

					vertices.push_back(a);
					vertices.push_back(b);
					vertices.push_back(cc);
					vertices.push_back(d);
					
					faces.push_back(glm::ivec4(vertexSize, vertexSize + 1, vertexSize + 2, vertexSize + 3));

					for (std::size_t b = 0; b < width; ++b)
						for (std::size_t a = 0; a < height; ++a)
							mask[counter + b + a * dims[u]] = 0;

					// Increment counters
					i += width; 
					counter += width;
				}
				else
				{
					++i;
					++counter;
				}
			}
		}
	}
}
