#include "game.hpp"
#include "chunk.hpp"
#include "geometry_optimizer.hpp"
#include "utils/Graphics/post_processing.hpp"
#include "GL/glew.h"
#include <set>

bool capture_mouse = true;
bool freeze_camera = false;
bool take_screenshot = false;
bool use_ssao = true;

Game::Game() :
fov(65.0f),
camera_near(0.1f),
camera_far(10000.0f),
free_camera(glm::vec3(0, 0, 0))
{
	free_camera.speed = glm::vec3(0.05f);
}
glm::mat4 Game::get_projection_matrix()const
{
	return glm::perspective(fov, resolution.x / resolution.y, camera_near, camera_far);
}
std::vector<Image> getSubImageRow(Image img, size_t leveys, size_t korkeus, size_t row, size_t bytesPerPixel)
{
	std::vector<unsigned char> &in = img.data;
	std::vector<Image> out;
	out.resize(img.width / leveys, Image(leveys, korkeus, 4, std::vector<unsigned char>()));

	size_t rowPointer = row*bytesPerPixel*img.width*korkeus;
	for(size_t y = 0; y<korkeus; ++y){
		size_t index = 0;
		for(size_t i = 0; i<img.width / leveys; ++i){
			out[index].data.reserve(korkeus*leveys*bytesPerPixel);
			copy(in.begin() + i*leveys*bytesPerPixel + rowPointer,
				in.begin() + (i + 1)*leveys*bytesPerPixel + rowPointer,
				std::back_insert_iterator<std::vector<unsigned char>>(out[index++].data));
		}
		rowPointer += bytesPerPixel*img.width;
	}
	return out;
}
std::vector<Image> getSubImages(Image img, size_t leveys, size_t korkeus, size_t bytesPerPixel)
{
	std::vector<Image> out;
	out.reserve(img.height / korkeus*img.width / leveys);

	for(size_t i = 0; i<img.height / korkeus; ++i){
		auto v = getSubImageRow(img, leveys, korkeus, i, bytesPerPixel);
		std::copy(v.begin(),
			v.end(),
			std::back_insert_iterator<std::vector<Image>>(out));
	}
	return out;
}
bool less(const glm::vec4& left, const glm::vec4& right)
{
	if (left.x == right.x){
		if (left.y == right.y)
			return left.z < right.z;
		else
			return left.y < right.y;
	}
	return left.x < right.x;
};
void Game::initialize(int resolution_width, int resolution_height)
{
	shader_file_watcher.initialize(SHADER_PATH + "shaders/");
	resolution = glm::vec2(resolution_width, resolution_height);
	glEnable(GL_FRAMEBUFFER_SRGB);
	glClearColor(133 / 255., 206 / 255., 250 / 255.,0);
	glViewport(0, 0, resolution_width, resolution_height);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW); //CCW is default
	glDepthFunc(GL_LEQUAL);

	shader_manager.load_shader("lighting", "shaders/lighting.vert", "shaders/lighting.frag");
	shader_manager.load_shader("face", "shaders/face.vert", "shaders/face.frag");
	shader_manager.load_shader("cube", "shaders/cube.vert", "shaders/cube.frag", "shaders/cube.geom");
	shader_manager.load_shader("mssao downsample", "shaders/post_processing_base.vert", "shaders/mssao_downsample.frag");
	shader_manager.load_shader("mssao blur", "shaders/post_processing_base.vert", "shaders/mssao_blur.frag");
	shader_manager.load_shader("mssao first", "shaders/post_processing_base.vert", "shaders/mssao_first.frag");
	shader_manager.load_shader("mssao", "shaders/post_processing_base.vert", "shaders/mssao.frag");
	shader_manager.load_shader("mssao last", "shaders/post_processing_base.vert", "shaders/mssao_last.frag");
	shader_manager.load_shader("compose", "shaders/post_processing_base.vert", "shaders/compose.frag");

	//create FBOs
	PostProcessConfig default_postprocess_config, default_postprocess_config2, shadow_postprocess_config;
	default_postprocess_config.add_texture(PostProcessTexture(resolution_width, resolution_height, TextureInternalFormat::RGBA16F));//color
	default_postprocess_config.add_texture(PostProcessTexture(resolution_width, resolution_height, TextureInternalFormat::RGBA));//normal + luma
	default_postprocess_config.add_depth_texture(PostProcessTexture(resolution_width, resolution_height, TextureInternalFormat::DEPTH_COMPONENT));
	default_postprocess.initialize(default_postprocess_config);

	glm::ivec2 resolution(resolution_width, resolution_height),
		half_resolution(resolution / 2),
		quarter_resolution(resolution / 4),
		eighth_resolution(resolution / 8),
		sixteenth_resolution(resolution / 16);
	half_resolution += glm::ivec2(half_resolution.x % 2, half_resolution.y % 2);
	quarter_resolution += glm::ivec2(quarter_resolution.x % 2, quarter_resolution.y % 2);
	eighth_resolution += glm::ivec2(eighth_resolution.x % 2, eighth_resolution.y % 2);
	sixteenth_resolution += glm::ivec2(sixteenth_resolution.x % 2, sixteenth_resolution.y % 2);

	half_downsample.initialize(half_resolution.x, half_resolution.y, TextureInternalFormat::RGBA, TextureInternalFormat::DEPTH_COMPONENT);
	quarter_downsample.initialize(quarter_resolution.x, quarter_resolution.y, TextureInternalFormat::RGBA, TextureInternalFormat::DEPTH_COMPONENT);
	eighth_downsample.initialize(eighth_resolution.x, eighth_resolution.y, TextureInternalFormat::RGBA, TextureInternalFormat::DEPTH_COMPONENT);
	sixteenth_downsample.initialize(sixteenth_resolution.x, sixteenth_resolution.y, TextureInternalFormat::RGBA, TextureInternalFormat::DEPTH_COMPONENT);
	mssao.initialize(resolution_width, resolution_height);
	{
		float vertices[] =
		{
			-1, -1,
			1, -1,
			-1, 1,
			1, 1
		};
		full_screen_quad.initialize(VertexBufferTarget::ARRAY_BUFFER);
		full_screen_quad.bufferData(sizeof(vertices), vertices, VertexBufferUsage::STATIC_DRAW);
	}
	//load models
	std::string models_to_load2[] =
	{
		"cube2.fbx",
		"cube.fbx"
	};
	std::vector<std::string> models_to_load(std::begin(models_to_load2), std::end(models_to_load2));
	auto textures = model_manager.load_models(models_to_load);
	textures.push_back(TextureDescriptor("minecraft.bmp", true));
	texture_manager.load_textures(textures, TextureMagFilter::NEAREST, TextureMinFilter::NEAREST);
	std::vector<glm::vec3> cube_positions;
	float width = 10;
	float depth = 10;
	float height = 64;
	cube_positions.reserve(static_cast<unsigned int>(width * depth * height));
	for(int x = 0; x < width; ++x){
		for(int z = 0; z < depth; ++z){
			for(int y = 0; y != 10; ++y){
				cube_positions.push_back(glm::vec3(
					x,
					y,
					//glm::floor(sin(x / width * 3.14) * sin(z / depth * 3.14) * height),
					z));
			}
		}
	}
	ChunkOptimizer chunk_optimizer;
	/*
	int n = 13 * 16;
	for(int i = 0; i != 5; ++i){
		for(int j = 0; j != 5; ++j){
			for(int k = 0; k != 5; ++k)
				chunk_optimizer << Chunk(glm::vec3(i, k, j)*glm::vec3(16,16,16), 15 * 16 + 2);
		}
	}
	*/
	//chunk_optimizer << Chunk(glm::vec3(0, 0, 0), 15 * 16 + 2) << Chunk(glm::vec3(16, 0, 0), 15 * 16 + 2)
		//<< Chunk(glm::vec3(0, 0, 16), 15 * 16 + 2) << Chunk(glm::vec3(16, 0, 16), 15 * 16 + 2) << Chunk(glm::vec3(32, 0, 0), 15 * 16 + 1);
	std::vector<glm::vec4> cube_positions2;
	std::vector<Face> cube_faces;
	std::vector<Vertex2> vertices;
	std::vector<unsigned int> indices;
	//chunk_optimizer.do_greedy(vertices, indices);
	std::ifstream file("points.txt");
	glm::vec4 point;
	
	std::set <glm::vec4, bool(*)(const glm::vec4&, const glm::vec4&)> pointset(less);
	while (file >> point.x >> point.y >> point.z >> point.w){
		pointset.insert(point);
		//cube_positions2.push_back(point);
	}
	
	cube_positions2.insert(cube_positions2.begin(), pointset.begin(), pointset.end());
	std::vector<Chunk> chunks;
	chunks.reserve(1600);
	Chunk::chunkify(cube_positions2, chunks);
	chunk_optimizer << chunks;
	chunk_optimizer.do_greedy(vertices, indices);
	std::cout << "CHUUUNKS=" << chunks.size() << std::endl;
	triangle_renderer.initialize(vertices, indices);
	//face_renderer.initialize(cube_faces);
	//CubeOptimizer op;
	//op << cube_positions2;
	//cube_positions2.clear();
	//op >> cube_positions2;
	//cube_renderer.initialize(cube_positions2);

	//load minecraft texture to texture array
	std::clog << "Loading textures...";
	Image img = texture_manager.load_image("textures/minecraft.bmp");
	auto v = getSubImages(img, 16, 16, 4);
	std::vector<unsigned char> textureCombined;
	textureCombined.reserve(v.size() * 16 * 16 * 4);
	for(auto iter = v.begin(); iter != v.end(); ++iter)
		copy(iter->data.begin(), iter->data.end(), std::back_insert_iterator<std::vector<unsigned char>>(textureCombined));

	glGenTextures(1, &texture_array_id);
	glBindTexture(GL_TEXTURE_2D_ARRAY, texture_array_id);
	int anistropy;
	glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anistropy);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_ANISOTROPY_EXT, std::min(16, anistropy));
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_SRGB_ALPHA, 16, 16, 256, 0, GL_BGRA, GL_UNSIGNED_BYTE, textureCombined.data());
	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
	
	std::clog << "done" << std::endl;

}
Game::~Game(){
	glDeleteTextures(1, &texture_array_id);
}

void Game::render()
{
	default_postprocess.begin_render_to_texture();
	Shader& lighting_shader = shader_manager.get_shader("lighting");
	Shader& cube_shader = shader_manager.get_shader("cube");
	Shader& face_shader = shader_manager.get_shader("face");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	lighting_shader.bind();
	lighting_shader["MVP"] = MVP;
	lighting_shader["inverse_transpose_transformation"] = inverse_MVP;
	lighting_shader["camera_position"] = free_camera.position;
	lighting_shader["light_position"] = glm::vec3(0, 200, 0);
	lighting_shader["sunlight_direction"] = glm::vec3(0, 1, 0);

	model_manager.get_model("cube.fbx").render(lighting_shader, texture_manager, glm::scale(glm::mat4(),glm::vec3(1,1,1)));

	cube_shader.bind();
	cube_shader["MVP"] = MVP;
	cube_shader["transformation"] = glm::mat4();
	cube_shader["sunlight_direction"] = glm::vec3(0, 1, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, texture_array_id);
	//cube_renderer.render(cube_shader, free_camera.position, free_camera.forw);


	face_shader.bind();
	face_shader["MVP"] = MVP;
	face_shader["transformation"] = glm::mat4();
	face_shader["sunlight_direction"] = glm::vec3(0, 1, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, texture_array_id);
	//face_renderer.render(face_shader, free_camera.position, free_camera.forw);
	triangle_renderer.render(face_shader, free_camera.position, free_camera.forw);
	default_postprocess.end_render_to_texture();

	render_postprocess();

}
void Game::render_postprocess()
{
	//setup common
	glActiveTexture(GL_TEXTURE0);
	full_screen_quad.bind();
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	if(use_ssao){
		Shader& downsample_shader = shader_manager.get_shader("mssao downsample");
		downsample_shader.bind();
		downsample_shader["inverse_MVP"] = inverse_MVP;

		half_downsample.downsample(downsample_shader, default_postprocess.get_textures()[1],
			default_postprocess.get_depth_texture(),
			[](){glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); });
		quarter_downsample.downsample(downsample_shader, default_postprocess.get_textures()[1],
			default_postprocess.get_depth_texture(),
			[](){glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); });
		eighth_downsample.downsample(downsample_shader, default_postprocess.get_textures()[1],
			default_postprocess.get_depth_texture(),
			[](){glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); });
		sixteenth_downsample.downsample(downsample_shader, default_postprocess.get_textures()[1],
			default_postprocess.get_depth_texture(),
			[](){glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); });

		mssao.calculate(texture_manager,
			default_postprocess.get_textures()[1],
			default_postprocess.get_depth_texture(),
			half_downsample.get_normal(),
			half_downsample.get_depth(),
			quarter_downsample.get_normal(),
			quarter_downsample.get_depth(),
			eighth_downsample.get_normal(),
			eighth_downsample.get_depth(),
			sixteenth_downsample.get_normal(),
			sixteenth_downsample.get_depth(),
			shader_manager.get_shader("mssao blur"),
			shader_manager.get_shader("mssao first"),
			shader_manager.get_shader("mssao"),
			shader_manager.get_shader("mssao last"),
			inverse_MVP,
			[](){glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); }, take_screenshot);
	}
	glViewport(0, 0, (int)resolution.x, (int)resolution.y);

	Shader& compose_shader = shader_manager.get_shader("compose");
	compose_shader.bind();
	compose_shader["use_ssao"] = (int)use_ssao;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glActiveTexture(GL_TEXTURE0);
	default_postprocess.get_texture().bind();
	glActiveTexture(GL_TEXTURE1);
	default_postprocess.get_textures()[1].bind();
	glActiveTexture(GL_TEXTURE2);
	mssao.get_result().bind();
	glActiveTexture(GL_TEXTURE3);
	default_postprocess.get_depth_texture().bind();
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}
void Game::update()
{
	static sf::Clock clock;
	const Time timestep(milliseconds(20));
	static Time last_frame_time, physics_time_accumulation;
	physics_time_accumulation += clock.getElapsedTime() - last_frame_time;

	while(physics_time_accumulation > timestep)
	{
		free_camera.update(timestep.asSeconds());
		auto view = free_camera.create_look_at();
		auto projection = get_projection_matrix();
		MVP = projection * view;
		inverse_MVP = glm::inverse(MVP);
		
		physics_time_accumulation -= timestep;
		model_manager.update_animations(timestep); //this shit doesnt work 
		if(physics_time_accumulation.asMilliseconds() > 1000)
			physics_time_accumulation = Time::Zero;
	}
	check_for_changed_files();
}
bool Game::is_requiring_mouse_capture()const
{
	return capture_mouse;
}
void Game::resize(int resolution_width, int resolution_height)
{

}
void Game::check_for_changed_files()
{
	static sf::Clock file_watcher_timer;
	if(file_watcher_timer.getElapsedTime().asSeconds() > 1){
		auto shaders = shader_file_watcher.get_changed_filenames();

		for(auto iter = shaders.begin(); iter != shaders.end(); ++iter)
			shader_manager.reload_shader("shaders/" + *iter);
		file_watcher_timer.restart();
	}
}
void Game::mouse_move(float deltax, float deltay)
{
	if(!freeze_camera)
		free_camera.mouse_move(deltax, deltay);
}
void Game::input(const sf::Event &event, sf::Window& window)
{
	if(!freeze_camera){
		free_camera.input(event);
		std::cout << Global::to_string(free_camera.position) << std::endl;
	}

	if(event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Num1)
		capture_mouse = !capture_mouse;
	if(event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Num3)
		freeze_camera = !freeze_camera;
	if(event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Num4)
		free_camera.speed *= 0.5;
	if(event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Num5)
		free_camera.speed *= 2;
	if(event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Num8)
		use_ssao = !use_ssao;
}
