#ifndef CHUNK_HPP
#define CHUNK_HPP
#include <glm/glm.hpp>
#include <vector>

class Chunk
{
public:
	static std::vector<Chunk> chunkify(const std::vector<glm::vec4>& cubes);
	static void chunkify(const std::vector<glm::vec4>& cubes, std::vector<Chunk>& existing_chunks);
	
	static const unsigned int WIDTH = 32, HEIGHT = 32, DEPTH = 32; //16*16*16=4096 blocks per chunk
	//15 * 16 + 2 for dirt
	Chunk(glm::vec3 position, unsigned char texture_id) : position(position), cubes(WIDTH*HEIGHT*DEPTH, texture_id), cubes_lightlevel(WIDTH*HEIGHT*DEPTH, 15){}

	glm::vec3 position;
	std::vector<glm::vec4> get_cubes()const;
	std::vector<glm::vec4> get_visible_cubes()const;
	std::vector<glm::vec4> get_cube_neighbours(glm::vec4 cube)const;
	glm::vec4 get_cube(glm::vec3 position)const;
	bool is_inside_chunk_absolute(glm::vec3 position)const; //-MAX_FLOAT to +MAX_FLOAT 
private:
	glm::vec4 get_cube_relative(glm::vec3 position)const;
	
	bool is_inside_chunk_relative(glm::vec3 position)const; //>0 & <widht/height/depth
	void set_cube(glm::vec4 cube);
	static glm::vec3 get_chunk_coordinate(const glm::vec4& position);

	std::vector<unsigned char> cubes;
	std::vector<unsigned char> cubes_lightlevel;
};

#endif
