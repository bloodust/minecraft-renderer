#ifndef CUBE_FACE_HPP
#define CUBE_FACE_HPP
#include "utils/Graphics/vertex.hpp"
#include <glm/glm.hpp>
#include <vector>

class Face
{
public:
	Face(glm::vec4 position, glm::vec3 normal);
	Face(glm::vec3 position, glm::vec3 normal, unsigned char texture_id);
	void get_geometry(std::vector<glm::vec4> &vertices, std::vector<glm::vec3> &normals, std::vector<glm::vec2>& tex_coords)const;
	glm::vec4 position;
	glm::vec3 normal;
};

#endif
