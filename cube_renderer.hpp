#ifndef CUBE_RENDERER_HPP
#define CUBE_RENDERER_HPP
#include "utils/Graphics/vertex.hpp"
#include "face.hpp"
#include "utils/Graphics/vertexbuffer.hpp"
#include "utils/Graphics/shader.hpp"
#include <glm/glm.hpp>

class PointRenderer
{
public:
	void initialize(const std::vector<glm::vec4>& cube_positions);
	void render(Shader& shader, glm::vec3 player_position, glm::vec3 player_forward)const;
private:
	VertexBuffer vbo;
	unsigned int number_of_cubes;
};

class FaceRenderer
{
public:
	void initialize(const std::vector<Face>& faces);
	void render(Shader& shader, glm::vec3 player_position, glm::vec3 player_forward)const;
private:
	VertexBuffer vbo;
	unsigned int number_of_triangles;

};

class TriangleRenderer
{
public:
	void initialize(const std::vector<Vertex2> &vertices, const std::vector<unsigned int> &indices);
	void render(Shader& shader, glm::vec3 player_position, glm::vec3 player_forward)const;
private:
	VertexBuffer vbo, ibo;
	unsigned int number_of_indices;
};
#endif
