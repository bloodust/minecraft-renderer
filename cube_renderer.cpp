#include "cube_renderer.hpp"
#include "utils/Graphics/opengl_error.hpp"

void PointRenderer::initialize(const std::vector<glm::vec4>& cube_positions)
{
	vbo.initialize(VertexBufferTarget::ARRAY_BUFFER);
	vbo.bufferData(cube_positions, VertexBufferUsage::STATIC_DRAW);
	number_of_cubes = cube_positions.size();
}
void PointRenderer::render(Shader& shader, glm::vec3 player_position, glm::vec3 player_forward)const
{
	print_error();
	shader.bind();
	print_error();
	shader["player_position"] = player_position;
	shader["player_forward"] = player_forward;

	vbo.bind();
	print_error();
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, false, sizeof(glm::vec4), nullptr); //cube positions
	glDrawArrays(GL_POINTS, 0, number_of_cubes);
	print_error();

}

void FaceRenderer::initialize(const std::vector<Face>& faces)
{
	vbo.initialize(VertexBufferTarget::ARRAY_BUFFER);
	struct Vertex
	{
		glm::vec4 position; glm::vec3 normal; glm::vec2 tex_coord;
	};
	std::vector<Vertex> vertices;
	vertices.reserve(faces.size() * 6);
	for (auto iter = faces.begin(); iter != faces.end(); ++iter){
		std::vector<glm::vec4> v;
		std::vector<glm::vec3> n;
		std::vector<glm::vec2> t;
		iter->get_geometry(v, n, t);
		Vertex v1 = { v[0], n[0], t[0] };
		Vertex v2 = { v[1], n[1], t[1] };
		Vertex v3 = { v[2], n[2], t[2] };
		Vertex v4 = { v[3], n[3], t[3] };
		Vertex v5 = { v[4], n[4], t[4] };
		Vertex v6 = { v[5], n[5], t[5] };

		vertices.push_back(v1);
		vertices.push_back(v2);
		vertices.push_back(v3);
		vertices.push_back(v4);
		vertices.push_back(v5);
		vertices.push_back(v6);
	}
	vbo.bufferData(vertices, VertexBufferUsage::STATIC_DRAW);
	number_of_triangles = vertices.size();
}
void FaceRenderer::render(Shader& shader, glm::vec3 player_position, glm::vec3 player_forward)const
{
	print_error();
	shader.bind();
	print_error();

	vbo.bind();
	print_error();
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 4, GL_FLOAT, false, sizeof(glm::vec4)+sizeof(glm::vec3)+sizeof(glm::vec2), nullptr); //positions
	glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(glm::vec4) + sizeof(glm::vec3) + sizeof(glm::vec2), (void*)sizeof(glm::vec4)); //normals
	glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(glm::vec4) + sizeof(glm::vec3) + sizeof(glm::vec2), (void*)(sizeof(glm::vec4) + sizeof(glm::vec3))); //tex coords
	glDrawArrays(GL_TRIANGLES, 0, number_of_triangles);
	print_error();

}


void TriangleRenderer::initialize(const std::vector<Vertex2> &vertices, const std::vector<unsigned int> &indices)
{
	vbo.initialize(VertexBufferTarget::ARRAY_BUFFER);
	vbo.bufferData(vertices, VertexBufferUsage::STATIC_DRAW);
	ibo.initialize(VertexBufferTarget::ELEMENT_ARRAY_BUFFER);
	ibo.bufferData(indices, VertexBufferUsage::STATIC_DRAW);
	number_of_indices = indices.size();
}
void TriangleRenderer::render(Shader& shader, glm::vec3 player_position, glm::vec3 player_forward)const
{
	print_error();
	shader.bind();
	print_error();

	vbo.bind();
	ibo.bind();
	print_error();
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 4, GL_FLOAT, false, sizeof(Vertex2), nullptr); //positions
	glVertexAttribPointer(1, 4, GL_FLOAT, false, sizeof(Vertex2), (void*)sizeof(glm::vec4)); //normals
	glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(Vertex2), (void*)(sizeof(glm::vec4) + sizeof(glm::vec4))); //tex coords
	glDrawElements(GL_TRIANGLES, number_of_indices, GL_UNSIGNED_INT, nullptr);
	print_error();

}
