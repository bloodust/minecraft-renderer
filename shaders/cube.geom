#version 330

layout(points) in;
layout(triangle_strip, max_vertices = 24) out;

uniform mat4 MVP;
uniform mat4 inverse_transpose_transformation;
uniform mat4 transformation;
uniform vec3 player_position;

out vec4 vertex;
out vec3 normal;
out vec2 tex_coord;

void emit_face(vec4 corner, vec3 face_normal, float texture_index)
{
	vec4 right = vec4(face_normal.z, face_normal.x, face_normal.y, 0);
	vec4 top = vec4(cross(face_normal, right.xyz), 0);

	normal = face_normal;

	vertex = corner;
	vertex.w = texture_index;
	tex_coord = vec2(0,0);
	gl_Position = MVP*(corner);
	EmitVertex();

	vertex = corner+right;
	vertex.w = texture_index;
	tex_coord = vec2(0,1);
	gl_Position = MVP*(corner+right);
	EmitVertex();

	vertex = corner+top;
	vertex.w = texture_index;
	tex_coord = vec2(1,0);
	gl_Position = MVP*(corner+top);
	EmitVertex();

	vertex = corner+right+top;
	vertex.w = texture_index;
	tex_coord = vec2(1,1);
	gl_Position = MVP*(corner+right+top);
	EmitVertex();

	EndPrimitive();
}
void main()
{
	
	vec4 position = transformation*(vec4(gl_in[0].gl_Position.xyz, 1));
	float texture_index = gl_in[0].gl_Position.w;

	vec3 camera_to_face = normalize(player_position-position.xyz);
	
	camera_to_face = normalize(player_position-(position+vec4(0,1,0,0)).xyz);
	if(dot(camera_to_face, vec3(0,1,0)) > 0)
		emit_face(position+vec4(0,1,0,0), vec3(0,1,0), texture_index);

	camera_to_face = normalize(player_position-(position+vec4(1,0,0,0)).xyz);
	if(dot(camera_to_face, vec3(1,0,0)) > 0)
		emit_face(position+vec4(1,0,0,0), vec3(1,0,0), texture_index);

	camera_to_face = normalize(player_position-(position+vec4(0,0,1,0)).xyz);
	if(dot(camera_to_face, vec3(0,0,1)) > 0)
		emit_face(position+vec4(0,0,1,0), vec3(0,0,1), texture_index);
		
		
	camera_to_face = normalize(player_position-(position+vec4(0,0,1,0)).xyz);
	if(dot(camera_to_face, vec3(0,-1,0)) > 0)
		emit_face(position+vec4(0,0,1,0), vec3(0,-1,0), texture_index);

	camera_to_face = normalize(player_position-(position+vec4(0,1,0,0)).xyz);
	if(dot(camera_to_face, vec3(-1,0,0)) > 0)
		emit_face(position+vec4(0,1,0,0), vec3(-1,0,0), texture_index);

	camera_to_face = normalize(player_position-(position+vec4(1,0,0,0)).xyz);
	if(dot(camera_to_face, vec3(0,0,-1)) > 0)
		emit_face(position+vec4(1,0,0,0), vec3(0,0,-1), texture_index);
		
}
