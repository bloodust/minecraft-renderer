#version 330
#extension GL_EXT_gpu_shader4 : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding=0) uniform sampler2DArray diffuse_texture;
layout(binding=1) uniform sampler2D specular_texture;
layout(binding=2) uniform sampler2D normal_texture;

uniform vec3 camera_position;
uniform vec3 sunlight_direction;
uniform vec3 light_position;

in vec4 vertex;
in vec3 normal;
in vec2 tex_coord;

layout(location=0) out vec4 color_out;
layout(location=1) out vec4 normal_out;

float distance_attenuation(vec3 vertex_pos, vec3 light_pos)
{
	vec3 light_dir = light_pos-vertex_pos;
	float distance_sqr = dot(light_dir,light_dir);
	return 1/(1.0+distance_sqr);
}
float angle_factor(vec3 vertex_pos, vec3 normal, vec3 light_pos)
{
	vec3 light_dir = normalize(vertex_pos-light_pos);
	return max(dot(normal, light_dir),0.0);
}
float angle_factor(vec3 normal, vec3 light_direction)
{
	return max(dot(normal,light_direction),0.0);
}
vec4 dither(vec4 color)
{
	uint i = uint(gl_FragCoord.x)%16u + 16u*(uint(gl_FragCoord.y)%16u);
	vec4 threshold = vec4((i*107u)%256u,(i*107u+5u)%256u,(i*107u+11u)%256u,(i*107u+3u)%256u)/256.;
	return color+(step(threshold,fract(color*255))-fract(color*255))/255.;
}

float blinn_factor(vec3 vertex_pos, vec3 normal, vec3 light_pos, vec3 camera_pos, float shininess)
{
	vec3 light_dir = normalize(light_pos-vertex_pos);
	float cos_angle_incidence = angle_factor(vertex_pos,normal,light_pos);
	vec3 view_dir = normalize(camera_pos-vertex_pos);

	vec3 half_angle = normalize(light_dir+view_dir);
	float blinn_term = max(dot(normal,half_angle),0);
	if(cos_angle_incidence == 0.0)
		blinn_term = 0.0;
	else
		blinn_term = pow(blinn_term,shininess);
	return blinn_term;
}
void main()
{
	//vec4 diffuse_color = texture(diffuse_texture, vec3(tex_coord, 15*16+2));
	vec4 diffuse_color = texture(diffuse_texture, vec3(tex_coord, vertex.w));
	vec4 specular_color = vec4(1,1,1,1);
	if(diffuse_color.a < 0.95)
		discard;
	//vec3 light_color = vec3(1,0.81,0.63);
	vec3 light_color = vec3(1,1,1);
	float ambient_light_intensity = 0.9;

	const float shininess = 128;

	vec3 normalized_normal = normal;
	vec3 end_color = diffuse_color.xyz * light_color * angle_factor(normalized_normal, sunlight_direction)
							+ diffuse_color.xyz * light_color * ambient_light_intensity;
	color_out.xyz = end_color;
	color_out.a = diffuse_color.a;
	
	//color_out = vec4(1,1,1,1);
	//color_out.xyz = normal/2+vec3(0.5);
	//color_out.xyz = tex_coord.xyy;
	//color_out = diffuse_color;
	//color_out = diffuse_color;
	normal_out.xyz = normalized_normal*0.5+0.5;
	if(tex_coord.x < 0.05 || tex_coord.x > 0.95 || tex_coord.y < 0.05 || tex_coord.y > 0.95)
		color_out = vec4(0.4);
}
