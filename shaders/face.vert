#version 330

layout(location=0) in vec4 vertex_in;
layout(location=1) in vec4 normal_in;
layout(location=2) in vec2 tex_coord_in;

uniform mat4 MVP;
uniform vec3 sunlight_direction;

out vec4 vertex;
out vec4 normal;
out vec2 tex_coord;
out vec3 light_multiplier;

float angle_factor(vec3 vertex_position, vec3 normal, vec3 light_position)
{
	vec3 light_direction = normalize(vertex_position-light_position);
	return max(dot(normal, light_direction), 0.0);
}
float angle_factor(vec3 normal, vec3 light_direction)
{
	return max(dot(normal, light_direction), 0.0);
}

void main()
{
	vertex = vertex_in;
	normal = normal_in;
	tex_coord = tex_coord_in;

	light_multiplier = vec3(angle_factor(normal.xyz, sunlight_direction));

	gl_Position = MVP*vec4(vertex_in.xyz, 1);
}
