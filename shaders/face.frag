#version 330
#extension GL_ARB_shading_language_420pack : enable
layout(binding=0) uniform sampler2DArray diffuse_texture;


in vec4 vertex;
in vec4 normal;
in vec2 tex_coord;
in vec3 light_multiplier;

layout(location=0) out vec4 color_out;
layout(location=1) out vec4 normal_out;

void main()
{
	vec4 diffuse_color = texture(diffuse_texture, vec3(tex_coord, vertex.w));
	if(diffuse_color.a < 0.95)
		discard;

	float ambient_light_intensity = 0.9;

	color_out.xyz = diffuse_color.xyz * light_multiplier
							+ diffuse_color.xyz * ambient_light_intensity;

	color_out.a = diffuse_color.a;
	normal_out = normal*0.5+0.5;
	color_out *= normal.w;
	if(tex_coord.x < 0.1 || tex_coord.y < 0.1 )
		color_out = vec4(0.4);
}
