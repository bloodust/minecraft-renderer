#include "game.hpp"
#include "utils/timer.hpp"
#include <Windows.h>
#include <GL/glew.h>
#include <iostream>
#include <SFML/Window.hpp>
#include <glm/glm.hpp>

const int RESOLUTION_WIDTH = 1280, RESOLUTION_HEIGHT = 720;
const float mouse_sensitivity = 0.6f;
template<typename T>
glm::detail::tvec2<T, glm::precision::defaultp> to_glm(typename sf::Vector2<T> v)
{
	return glm::detail::tvec2<T, glm::precision::defaultp>(v.x, v.y);
}
void uncapture_mouse()
{
	ClipCursor(nullptr);
	ReleaseCapture();
}

void capture_mouse(sf::Window &window)
{
	SetCapture(window.getSystemHandle());
}

int main(int argc, char **argv)
{
	sf::Window window(sf::VideoMode(RESOLUTION_WIDTH, RESOLUTION_HEIGHT, 32), "Test");//, sf::Style::Fullscreen);

	window.setPosition(sf::Vector2i(0, 0));
	window.setKeyRepeatEnabled(false);
	auto e = glewInit();
	if(e != GLEW_OK){
		std::cerr << "Failed to initialize glew" << std::endl;
		std::cerr << glewGetErrorString(e) << std::endl;
		return 1;
	}
	Game game;
	try{
		Timer t;
		std::cout << "Loading..." << std::endl;
		t.start();
		game.initialize(RESOLUTION_WIDTH, RESOLUTION_HEIGHT);
		t.stop();
		std::cout << "Loaded in " << std::fixed << t.getElapsedTimeInSec() << " seconds " << std::endl;
		window.setVerticalSyncEnabled(false);
		//return 0;
		while(window.isOpen())
		{
			window.setActive();
			// Process events
			sf::Event event;
			while(window.pollEvent(event))
			{
				if(event.type == sf::Event::MouseMoved){
					static int x = event.mouseMove.x, y = event.mouseMove.y;
					int deltax = event.mouseMove.x - x;
					int deltay = event.mouseMove.y - y;
					game.mouse_move(deltax*mouse_sensitivity, deltay*mouse_sensitivity);
					x = event.mouseMove.x;
					y = event.mouseMove.y;

					if(game.is_requiring_mouse_capture()){
						//move mouse back to the center of the screen
						sf::Vector2i window_size(window.getSize().x, window.getSize().y);
						if(sf::Mouse::getPosition(window) != window_size / 2){
							x = window_size.x / 2;
							y = window_size.y / 2;
							sf::Mouse::setPosition(window_size / 2, window);
						}
						capture_mouse(window);
						window.setMouseCursorVisible(false);
					}
					else{
						uncapture_mouse();
						window.setMouseCursorVisible(true);
					}
				}
				else
					game.input(event, window);

				if(event.type == sf::Event::Closed){
					window.close();
					return 0;
				}
				if((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape)){
					window.close();
					return 0;
				}
				if(event.type == sf::Event::Resized){
					game.resize(window.getSize().x, window.getSize().y);
					break;
				}
			}
			game.render();
			game.update();
			window.display();
		}
	}
	catch(std::exception& exception)
	{
		std::cerr << exception.what() << std::endl;
		assert(false);
	}
	catch(...)
	{
		std::cerr << "Unknown error" << std::endl;
		assert(false);
	}
	return 0;
}
